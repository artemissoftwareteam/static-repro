# What is this?

A minimal Conflunce static macro cloud add-on built with ACE v3.2 that demonstrates some buggy Confluence behaviors, including:

1. Multiple requests made by Confluence to the add-on server per static macro on a page (only one request is expected).  This can be demonstrated in both old (v1) and new (v2) Confluence editors for both `GET` and `POST` rendering methods.
1. In the new (v2) Confluence editor, the second request (as described in #1 above) contains the _wrong_ macro ID for the `GET` rendering method.  For add-ons that must request the macro body from the Confluence REST API (rather than receiving the body using the `POST` rendering method), this is at least a confusing, if not also rather serious problem.  In the case of static macros using the `POST` rendering method in the v2 editor, two or more reqeusts are made and the last of them again has a different ID, with the difference from the `GET` case being that **both** IDs can be used to fetch the macro body from the Conflunce REST API.  This is, to say the least, extremely confusing and at best slightly worrying given the broken behavior of the `GET` case, wherein the second ID yields a `404 Not Found` response.
1. Racey caching of previously rendered macro body content when publishing a page in the new (v2) editor.
1. `Cache-Control` and `Etag` headers are not honored for `GET` requests by Confluence, despite claims in the Atlassian Connect documentation to the contrary.
  * It _may_ serendipitously be benficial that `Cache-Control` isn't honored, since the macro ID (and deprcated macro hash, which seems to be identical to the macro ID now) _does not seem to change_ when the content body changes.  According to the Connect documentation (which may have been written when Conflunce sent a macro hash instead of a macro ID), effective HTTP caching of static macro responses which provide a time-basd `Cache-Control` response header would rely on the rotating ID (or hash) as a cache busting tool.
  * That said, `Etag` based caching should still be possible (by tracking a combination of space key, page id, page version, and macro ID between requests), yet Confluence never seems to send an `If-None-Match` header in subsequent requests for the same macro content, despite claims in the AC documentation that it will.
1. Inline dynamic content macros seem to have a hard max width of 300px and cannot be resized to a wider width using the `AP.resize()` function inside the iframe.

## Setup and run

* Begin by making sure you have Node.js v10.13 (LTS) or later installed.  Earlier versions of Node may also work, but they have not been tested with this repository -- YMMV.
* Install dependencies with `npm install`.

### Run with Docker

* Make sure you have `docker` and `docker-compose` installed on your system.
* You will also need a paid ngrok account using the current configuration.
  * If you don't have one, either modify the `docker-compose` configuration to work with arbitrary subdomains (left as an exercise for the reader) or run and test as you would any other ACE application.
* Copy `.env.sample` to `.env`.
* Edit `.env` to set your own values for `NGROK_AUTH` and `NGROK_SUBDOMAIN`.
* Run `docker-compose up` to launch the server.
* Install the add-on in a Confluence cloud test instance using the url `https://<your-ngrok-subdomain>.ngrok.io/atlassian-connect.json`.

### Run without Docker

* Follow typical Atlassian Connect documentation in order to run locally using your own resource configuration.
* Install the add-on in a Confluence cloud test instance using the url `https://<your-ngrok-subdomain>.ngrok.io/atlassian-connect.json`.

## Issue demonstrations

Once the add-on is running and installed, follow these steps to reproduce the issues outlined in at the beginning of this document:

### Multiple requests, v1 editor

Confluence makes at least _2_ (and occasionally _more_!) requests for static macro content rather than just 1 as expected.  For pages using the v1 editor, the requests are identical, so the macro works without issue despite putting double the necessary load on the add-on server.  In the v2 example later on in this document, however, you will see that the second of the two requests is made with a non-existent ID, which is a much more serious problem.

#### Rendering method: GET

1. Create a new page in Confluence that still uses the v1 editor (at the time of this writing, a standard blank pag will do).
1. Create an instance of the `static-get` macro and give it any text body content.
1. Publish the page.
1. Clear your add-on logs in the terminal in which you started the web server.
1. Refresh the page containing the add-on.  Notice that at least _2_ `GET` requests were made for the static macro content rather than just one.  You will see output similar to the following:

```shell
node-static_1        | GET /static-get?sk=SMI&pi=4423682&pv=2&mi=5b524d4e-29f2-4a31-b5e8-eea3907a7060&mn=GET+1&xdm_e=https%3A%2F%2Fartemis-dev2.atlassian.net&xdm_c=channel-static-get&cp=%2Fwiki&xdm_deprecated_addon_key_do_not_use=static-repro&lic=none&cv=1.254.0 200 1689ms - 33b
node-static_1        | GET /static-get?sk=SMI&pi=4423682&pv=2&mi=5b524d4e-29f2-4a31-b5e8-eea3907a7060&mn=GET+1&xdm_e=https%3A%2F%2Fartemis-dev2.atlassian.net&xdm_c=channel-static-get&cp=%2Fwiki&xdm_deprecated_addon_key_do_not_use=static-repro&lic=none&cv=1.254.0 200 1250ms - 33b
```

#### Rendering method: POST

1. Set the environment variable `NODE_DEBUG=macro-requests` (if using the `docker-compose` method of running the add-on, set this in your `.env` file).
1. Create a new page in Confluence that still uses the v1 editor (at the time of this writing, a standard blank pag will do).
1. Create an instance of the `static-post` macro and give it any text body content.
1. Publish the page.
1. Clear your add-on logs in the terminal in which you started the web server.
1. Refresh the page containing the add-on.  Notice that at least _2_ `POST` requests were made for the static macro content rather than just one.  You will see output similar to the following:

```shell
node-static_1        | MACRO-REQUESTS 1: ---> POST:
node-static_1        | > body: { mn: 'POST 1',
node-static_1        |   cv: '1.254.0',
node-static_1        |   mb: '<p>some static post content</p>',
node-static_1        |   xdm_deprecated_addon_key_do_not_use: 'static-repro',
node-static_1        |   pv: '3',
node-static_1        |   lic: 'none',
node-static_1        |   sk: 'SMI',
node-static_1        |   pi: '4423682',
node-static_1        |   mh: 'a7f929b2-fcdb-45f4-ba5f-1f315b4fecd5',
node-static_1        |   mi: 'a7f929b2-fcdb-45f4-ba5f-1f315b4fecd5',
node-static_1        |   xdm_e: 'https://artemis-dev2.atlassian.net',
node-static_1        |   cp: '/wiki',
node-static_1        |   xdm_c: 'channel-static-post',
node-static_1        |   __proto__: {} }
node-static_1        | > headers: { authorization:
node-static_1        |    'JWT <omitted>',
node-static_1        |   'content-type': 'application/x-www-form-urlencoded',
node-static_1        |   via: '1.1 localhost (Apache-HttpClient/4.4.1 (cache))',
node-static_1        |   'transfer-encoding': 'chunked',
node-static_1        |   host: 'static-repro-demo.ngrok.io',
node-static_1        |   'user-agent':
node-static_1        |    'Atlassian HttpClient unknown / Confluence-1000.0.0-09b635b7104 (6452) / Atlassian-Connect/1.254.0',
node-static_1        |   'x-forwarded-proto': 'https',
node-static_1        |   'x-forwarded-for': '104.192.138.232' }
node-static_1        | MACRO-REQUESTS 1: out:
node-static_1        | <p>some static post content</p>
node-static_1        | POST /static-post 200 57ms - 31b
node-static_1        | MACRO-REQUESTS 1: ---> POST:
node-static_1        | > body: { mn: 'POST 1',
node-static_1        |   cv: '1.254.0',
node-static_1        |   mb: '<p>some static post content</p>',
node-static_1        |   xdm_deprecated_addon_key_do_not_use: 'static-repro',
node-static_1        |   pv: '3',
node-static_1        |   lic: 'none',
node-static_1        |   sk: 'SMI',
node-static_1        |   pi: '4423682',
node-static_1        |   mh: 'a7f929b2-fcdb-45f4-ba5f-1f315b4fecd5',
node-static_1        |   mi: 'a7f929b2-fcdb-45f4-ba5f-1f315b4fecd5',
node-static_1        |   xdm_e: 'https://artemis-dev2.atlassian.net',
node-static_1        |   cp: '/wiki',
node-static_1        |   xdm_c: 'channel-static-post',
node-static_1        |   __proto__: {} }
node-static_1        | > headers: { authorization:
node-static_1        |    'JWT <omitted>',
node-static_1        |   'content-type': 'application/x-www-form-urlencoded',
node-static_1        |   via: '1.1 localhost (Apache-HttpClient/4.4.1 (cache))',
node-static_1        |   'transfer-encoding': 'chunked',
node-static_1        |   host: 'static-repro-demo.ngrok.io',
node-static_1        |   'user-agent':
node-static_1        |    'Atlassian HttpClient unknown / Confluence-1000.0.0-09b635b7104 (6452) / Atlassian-Connect/1.254.0',
node-static_1        |   'x-forwarded-proto': 'https',
node-static_1        |   'x-forwarded-for': '104.192.138.232' }
node-static_1        | MACRO-REQUESTS 1: out:
node-static_1        | <p>some static post content</p>
node-static_1        | POST /static-post 200 8ms - 31b
```

### Multiple requests, v2 editor

In the v2 editor, the last request (of at least _2_) made for macro content _always_ seems to contain an invalid macro ID (that is, one which will result in a `404 Not Found` response from Confluence's REST API of the form `/content/{page}/history/{version}/macro/id/{id}`).

#### Rendering method: GET

1. Create a new page in Confluence that still uses the v2 editor (at the time of this writing, either a "Meeting notes" or "Blog post" blueprint will work).
2. Create an instance of the `static-get` macro and give it any text body content.
3. Publish the page and notice that the macro renders an error, similar to the following: `ERROR: Multiexcerpt "Test GET in v2" not found.`
4. Clear your add-on logs in the terminal in which you started the web server.
5. Refresh the page containing the add-on.  Notice that at least _2_ `GET` requests were made for the static macro content rather than just one.  You will see output similar to the following:

```shell
node-static_1        | GET /static-get?sk=SMI&pi=4423690&pv=1&mi=583929d1-c2f4-44ea-8cfc-c16e928f1b65&mn=GET+2&xdm_e=https%3A%2F%2Fartemis-dev2.atlassian.net&xdm_c=channel-static-get&cp=%2Fwiki&xdm_deprecated_addon_key_do_not_use=static-repro&lic=none&cv=1.254.0 200 2034ms - 29b
node-static_1        | GET /static-get?sk=SMI&pi=4423690&pv=1&mi=583929d1-c2f4-44ea-8cfc-c16e928f1b65&mn=GET+2&xdm_e=https%3A%2F%2Fartemis-dev2.atlassian.net&xdm_c=channel-static-get&cp=%2Fwiki&xdm_deprecated_addon_key_do_not_use=static-repro&lic=none&cv=1.254.0 200 1947ms - 29b
node-static_1        | GET /static-get?sk=SMI&pi=4423690&pv=1&mi=517aac75-8f4b-4acd-aa7c-554cb49adced&mn=GET+2&xdm_e=https%3A%2F%2Fartemis-dev2.atlassian.net&xdm_c=channel-static-get&cp=%2Fwiki&xdm_deprecated_addon_key_do_not_use=static-repro&lic=none&cv=1.254.0 200 920ms - 203b
```

**Notice that the last request passes a different macro ID than the first two did!**  Requesting this ID from the Conflunce REST API for the form `/content/{page}/history/{version}/macro/id/{id}` results in a `404 Not Found` HTTP error.

This flow is simply broken for the v2 editor with no known work around aside from using `renderingMethod: 'POST'` in the `atlassian-connect.json` descriptor for the macro.  While using `POST` is probably an accptable work around for most macros, any macro that stores the ID for later use (as in the Artemis Multiexcerpt macro) must now do extra work to validate the ID before storing it.  This "extra work" essentially amounts to requesting the macro body from the Conflunce REST API to see if it returns a valid `200` or `404` response.  _Doing this in fact defeats much of the point of using the `POST` rendering method in the first place!_

If you want to see deeper debugging output for the incoming requests and the requests made back to Confluence, you can set the following environment variable (if using the Docker configuration to run the add-on, set this in your `.env` file): `NODE_DEBUG=request,macro-requests`.  If using `ngrok` (which is part of the Docker configuration), you can also inspect the incoming HTTP requests and responses that Confluence initiates with the add-on in the `ngrok` HTTP debugging console running at `localhost:4040`.

Also notice that reloading the page again with no other changes will result in a final request with an entirely new ID.  That is, the first request (or requests) made while loading the page are made with the correct ID, consistent with the previous page load, but the final request in the series ends with a _different yet still invalid_ ID than in the previous page load.

```
node-static_1        | GET /static-get?sk=SMI&pi=4423690&pv=1&mi=583929d1-c2f4-44ea-8cfc-c16e928f1b65&mn=GET+2&xdm_e=https%3A%2F%2Fartemis-dev2.atlassian.net&xdm_c=channel-static-get&cp=%2Fwiki&xdm_deprecated_addon_key_do_not_use=static-repro&lic=none&cv=1.254.0 200 730ms - 29b
node-static_1        | GET /static-get?sk=SMI&pi=4423690&pv=1&mi=583929d1-c2f4-44ea-8cfc-c16e928f1b65&mn=GET+2&xdm_e=https%3A%2F%2Fartemis-dev2.atlassian.net&xdm_c=channel-static-get&cp=%2Fwiki&xdm_deprecated_addon_key_do_not_use=static-repro&lic=none&cv=1.254.0 200 588ms - 29b
node-static_1        | GET /static-get?sk=SMI&pi=4423690&pv=1&mi=044063e5-364f-4b2f-9a2d-2672d57b8261&mn=GET+2&xdm_e=https%3A%2F%2Fartemis-dev2.atlassian.net&xdm_c=channel-static-get&cp=%2Fwiki&xdm_deprecated_addon_key_do_not_use=static-repro&lic=none&cv=1.254.0 200 991ms - 62b
```

#### Rendering method: POST

`POST` requests exhibit this same behavior, but it's possible to work around the end user error by always responding with.

1. Set the environment variable `NODE_DEBUG=macro-requests` (if using the `docker-compose` method of running the add-on, set this in your `.env` file).
1. Create a new page in Confluence that still uses the v1 editor (at the time of this writing, a standard blank pag will do).
1. Create an instance of the `static-post` macro and give it any text body content.
1. Publish the page.
1. Clear your add-on logs in the terminal in which you started the web server.
1. Refresh the page containing the add-on.  Notice that at least _2_ `POST` requests were made for the static macro content rather than just one.  You will see output similar to the following:

```shell
node-static_1        | MACRO-REQUESTS 1: ---> POST:
node-static_1        | > body: { mn: 'POST 2',
node-static_1        |   cv: '1.254.0',
node-static_1        |   mb: '<p>new editor POST example</p>',
node-static_1        |   xdm_deprecated_addon_key_do_not_use: 'static-repro',
node-static_1        |   pv: '4',
node-static_1        |   lic: 'none',
node-static_1        |   sk: 'SMI',
node-static_1        |   pi: '4423690',
node-static_1        |   mh: '63d18c45-64be-4740-b192-7eef89ffac12',
node-static_1        |   mi: '63d18c45-64be-4740-b192-7eef89ffac12',
node-static_1        |   xdm_e: 'https://artemis-dev2.atlassian.net',
node-static_1        |   cp: '/wiki',
node-static_1        |   xdm_c: 'channel-static-post',
node-static_1        |   __proto__: {} }
node-static_1        | > headers: { authorization:
node-static_1        |    'JWT <omitted>',
node-static_1        |   via: '1.1 localhost (Apache-HttpClient/4.4.1 (cache))',
node-static_1        |   'transfer-encoding': 'chunked',
node-static_1        |   host: 'static-repro-demo.ngrok.io',
node-static_1        |   'user-agent':
node-static_1        |    'Atlassian HttpClient unknown / Confluence-1000.0.0-09b635b7104 (6452) / Atlassian-Connect/1.254.0',
node-static_1        |   'x-forwarded-proto': 'https',
node-static_1        |   'x-forwarded-for': '104.192.138.232' }
node-static_1        | MACRO-REQUESTS 1: 63d18c45-64be-4740-b192-7eef89ffac12 is valid
node-static_1        | MACRO-REQUESTS 1: out:
node-static_1        | <p>new editor POST example</p><p><em>[63d18c45-64be-4740-b192-7eef89ffac12 is valid]</em></p>
node-static_1        | POST /static-post 200 16ms - 93b
node-static_1        | POST /static-post 200 9ms - 93b
node-static_1        | MACRO-REQUESTS 1: ---> POST:
node-static_1        | > body: { mn: 'POST 2',
node-static_1        |   cv: '1.254.0',
node-static_1        |   mb: '<p>new editor POST example</p>',
node-static_1        |   xdm_deprecated_addon_key_do_not_use: 'static-repro',
node-static_1        |   pv: '4',
node-static_1        |   lic: 'none',
node-static_1        |   sk: 'SMI',
node-static_1        |   pi: '4423690',
node-static_1        |   mh: '63d18c45-64be-4740-b192-7eef89ffac12',
node-static_1        |   mi: '63d18c45-64be-4740-b192-7eef89ffac12',
node-static_1        |   xdm_e: 'https://artemis-dev2.atlassian.net',
node-static_1        |   cp: '/wiki',
node-static_1        |   xdm_c: 'channel-static-post',
node-static_1        |   __proto__: {} }
node-static_1        | > headers: { authorization:
node-static_1        |    'JWT <omitted>',
node-static_1        |   'content-type': 'application/x-www-form-urlencoded',
node-static_1        |   via: '1.1 localhost (Apache-HttpClient/4.4.1 (cache))',
node-static_1        |   'transfer-encoding': 'chunked',
node-static_1        |   host: 'static-repro-demo.ngrok.io',
node-static_1        |   'user-agent':
node-static_1        |    'Atlassian HttpClient unknown / Confluence-1000.0.0-09b635b7104 (6452) / Atlassian-Connect/1.254.0',
node-static_1        |   'x-forwarded-proto': 'https',
node-static_1        |   'x-forwarded-for': '104.192.138.232' }
node-static_1        | MACRO-REQUESTS 1: 63d18c45-64be-4740-b192-7eef89ffac12 is valid
node-static_1        | MACRO-REQUESTS 1: out:
node-static_1        | <p>new editor POST example</p><p><em>[63d18c45-64be-4740-b192-7eef89ffac12 is valid]</em></p>
node-static_1        | MACRO-REQUESTS 1: ---> POST:
node-static_1        | > body: { mn: 'POST 2',
node-static_1        |   cv: '1.254.0',
node-static_1        |   mb: '<p>new editor POST example</p>',
node-static_1        |   xdm_deprecated_addon_key_do_not_use: 'static-repro',
node-static_1        |   pv: '4',
node-static_1        |   lic: 'none',
node-static_1        |   sk: 'SMI',
node-static_1        |   pi: '4423690',
node-static_1        |   mh: '30ae9154-5f3e-457d-9d35-2915eb40b440',
node-static_1        |   mi: '30ae9154-5f3e-457d-9d35-2915eb40b440',
node-static_1        |   xdm_e: 'https://artemis-dev2.atlassian.net',
node-static_1        |   cp: '/wiki',
node-static_1        |   xdm_c: 'channel-static-post',
node-static_1        |   __proto__: {} }
node-static_1        | > headers: { authorization:
node-static_1        |    'JWT <omitted>',
node-static_1        |   'content-type': 'application/x-www-form-urlencoded',
node-static_1        |   via: '1.1 localhost (Apache-HttpClient/4.4.1 (cache))',
node-static_1        |   'transfer-encoding': 'chunked',
node-static_1        |   host: 'static-repro-demo.ngrok.io',
node-static_1        |   'user-agent':
node-static_1        |    'Atlassian HttpClient unknown / Confluence-1000.0.0-09b635b7104 (6452) / Atlassian-Connect/1.254.0',
node-static_1        |   'x-forwarded-proto': 'https',
node-static_1        |   'x-forwarded-for': '104.192.138.232' }
node-static_1        | MACRO-REQUESTS 1: 30ae9154-5f3e-457d-9d35-2915eb40b440 is valid
node-static_1        | MACRO-REQUESTS 1: out:
node-static_1        | <p>new editor POST example</p><p><em>[30ae9154-5f3e-457d-9d35-2915eb40b440 is valid]</em></p>
node-static_1        | POST /static-post 200 14ms - 93b
```

### Racey caching of previous macro body content in the v2 editor

Editing the content of a static macro body in the v2 editor usually results in the previous version of the macro body being displayed until the page is refreshed.  Occasionally the new content will be displayed instead.

TODO: full reproducer steps

### Cache-Control and Etag not honored

Despite the Atlassian Connect documentation's enouragement to leverage HTTP caching for static macro responses, we have never seen doing so have an effect.  See the relevant documentation [here](https://developer.atlassian.com/cloud/confluence/modules/static-content-macro).

#### Cache-Control

In the case of `Cache-Control`, setting `max-age` values do not seem to affect a Confluence-side HTTP cache in our experience, always resulting in subsequent requests for the same content.  That said, this was a more interesting feature when the macro hash was used (years ago, before the macro id was introduced and macro hash was deprecated).  The current Atlassian Connect documentation appears to reflect this old approach, in this quote:

> Because we declare the macro hash and parameters as URL variables, the URL will automatically change when the macro is changed.

Now, using the macro ID instead of the hash, if a macro doesn't include the page version (and assuming the page version is incremented whenever a macro body changes) in its URL template, then returning a `Cache-Control` header with a `max-age` would probably result in overly-aggressive caching.  When page version is included the in the url, then perhaps it would still be a viable tool... if it seemed to have any affect.

**TODO: full reproducer steps**

#### Etag/If-None-match

In the case of `Etag` headers, we have never seen Confluence make a follow up request for the same static macro that provides an `If-None-Match` header.  This is disappointing given that the Atlassian Connect documentation is so strongly worded to encourage the use of HTTP caching fundamentals, yet doesn't actually seem to implement it (and never has as far as we have seen over the multi-year lifespan of our commercial static macro add-on).

Of course, it's possible that we aren't doing right -- if this is the case, though, we would love to be shown some working examples, especially one built using ACE.

**TODO: full reproducer steps**

### 300px limit on dynamic inline macros

Dynamic content macros with `bodyType` equal to `inline` seem to be limited to a maximum width of `300px`, regardless of what width values we pass to `AP.resize(...)`.

Reported previously [here](https://community.developer.atlassian.com/t/dynamic-content-macro-stopped-to-render-correctly-when-iframe-width-is-100/5207) without a satisfying response.

**TODO: full reproducer steps**

## Notes

* Be aware that the act-as-user impl in this repository is currently broken, though we don't believe that it has any appreciable impact on the issue demonstrations, as the dfault add-on user permissions still work for typical test space and page permission configurations.
