// This is the entry point for your add-on, creating and configuring
// your add-on HTTP server

// [Express](http://expressjs.com/) is your friend -- it's the underlying
// web framework that `atlassian-connect-express` uses
var express = require('express');
// You need to load `atlassian-connect-express` to use her godly powers
var ac = require('atlassian-connect-express');
// Static expiry middleware to helpe serve static resources efficiently
var env = process.env;
env.PWD = env.PWD || process.cwd(); // Fix expiry on Windows :(
// We use [Handlebars](http://handlebarsjs.com/) as our view engine
// via [express-hbs](https://npmjs.org/package/express-hbs)
var hbs = require('express-hbs');
// We also need a few stock Node modules
var http = require('http');

// Anything in ./views are HBS templates
var viewsDir = __dirname + '/views';
// Your routes live here; this is the C in MVC
var routes = require('./routes');
// Bootstrap Express
var app = express();
// Bootstrap the `atlassian-connect-express` library
var addon = ac(app);
// You can set this in `config.js`
var port = addon.config.port();
// Server mode check
var serverEnv = app.get('env');
var isProduction = serverEnv === 'production';

// The following settings applies to all environments
app.set('port', port);

// Configure the Handlebars view engine
app.engine('hbs', hbs.express3({partialsDir: viewsDir}));
app.set('view engine', 'hbs');
app.set('views', viewsDir);

// Declare any Express [middleware](http://expressjs.com/api.html#middleware) you'd like to use here
app.use(express.favicon());
// Log requests, using an appropriate formatter by env
app.use(express.logger(isProduction ? 'default' : 'dev'));
// Include stock request parsers
app.use(express.bodyParser());
app.use(express.cookieParser());
// Gzip responses when appropriate
app.use(express.compress());
// You need to instantiate the `atlassian-connect-express` middleware in order to get its goodness for free
app.use(addon.middleware());
// Redirect GETs to https in production
if (isProduction) {
  app.get('*', function (req, res, next) {
    if (req.headers['x-forwarded-proto'] !== 'https') {
      return res.redirect(addon.config.localBaseUrl() + req.url)
    } else {
      // Due to an apparent AC bug, let's make sure the hostBaseUrl is https
    }
    next();
  });
}
// Mount the add-on's routes
app.use(app.router);

if (!isProduction) {
  // Show nicer errors when in dev mode
  app.use(express.errorHandler());
}

// Wire up your routes using the express and `atlassian-connect-express` objects
routes(app, addon);

// Boot the damn thing
http.createServer(app).listen(port, function () {
  console.log('Add-on server running at ' + process.env.LOCAL_BASE_URL);
});
