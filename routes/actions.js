var { inspect } = require('util');
var rest = require('../lib/rest');
var { loadClientInfo, macroFail, debug, etag } = require('../lib/util');

var useV2Workaround = process.env.USE_V2_WORKAROUND === 'true';
var httpCacheEnabled = process.env.HTTP_CACHE_ENABLED === 'true';
var httpEtagsEnabled = process.env.HTTP_ETAGS_ENABLED === 'true';

var etags = {};

module.exports = function (app, addon) {

  app.post(
    '/static-post',
    addon.authenticate(),
    loadClientInfo(addon),
    function (req, res) {
      debug('---> POST:\n> body: %s\n> headers: %s', inspect(req.body), inspect(req.headers));
      handleRequest(req, res);
    }
  );

  app.get(
    '/static-get',
    addon.authenticate(),
    loadClientInfo(addon),
    function (req, res) {
      debug('---> GET:\n> query: %s\n> headers: %s', inspect(req.query), inspect(req.headers));
      handleRequest(req, res);
    }
  );

  function handleRequest(req, res) {
    var mePageId = req.param('pi');
    var mePageVersion = req.param('pv');
    var meContentId = req.param('mi');
    var meName = req.param('mn');
    if (!mePageId) return res.send(400, 'Param pi is required');
    if (!mePageVersion) return res.send(400, 'Param pv is required');
    if (!meName) return res.send(400, 'Param mn is required');
    if (!meContentId) return res.send('No preview available');
    var meBody = req.body && req.body.mb;

    function processBody(body, idIsValid = true) {
      // imagine we asynchronously process the body in some way, recording the ID if `idIsValid === true`
      const idMessage = `${meContentId} is ${idIsValid ? '' : 'in'}valid`;
      debug(idMessage);
      return Promise.resolve(body + `<p><em>[${idMessage}]</em></p>`);
    }

    function handleContent(content) {
      debug('out:\n%s', content);
      if (httpCacheEnabled) {
        res.setHeader('Cache-Control', ['max-age=3600', 's-maxage=3600']);
      }
      if (httpEtagsEnabled) {
        const key = `${mePageId}:${mePageVersion}:${meContentId}`;
        etags[key] = etag(mePageId, mePageVersion, meContentId);
        res.setHeader('Etag', etags[key]);
      }
      res.send(content);
    }

    // In the v2 editor, `renderingMethod: "GET"` macros are completely broken.  The following
    // workaround demonstrates two different approaches to handling `renderingMethod: "POST"`
    // macros.  The difference may seem trivial for this macro, since the macro is essentially
    // uninteresting.  Assume for the sake of reading the following two clauses that the macro
    // has a vested interest in ensuring that the macro ID is valid and then recording it for
    // later use before resonding to the request.
    if (useV2Workaround) {
      // this is a workaround for validating the macro ID when posted via the new v2 editor
      getMacroBody(req.context, mePageId, mePageVersion, meContentId).then(function (result) {
        debug('request body:\n%s', result.body.body);
        // both types of renderingMethod (GET and POST) will reach this if the macro ID is valid
        return processBody(result.body.body);
      }).then(handleContent).catch(function (err) {
        if (meBody && err.code === 404) {
          // renderingMethod:POST requests that 404 on the macro ID should always pass through here
          debug('using posted macro body even though the id is invalid');
          // respond with the POSTED body, even if the ID was invalid (but don't record the ID)
          processBody(meBody, false).then(handleContent).catch(macroFail(res, meName));
        } else {
          // the last renderingMethod:GET request in the v2 editor will always end up here
          macroFail(res, meName)(err);
        }
      });
    } else {
      // If Atlassian fixes the v2 editor, use the following code instead of the workaround, as it will perform better!
      // Sadly we can't currently do this since it assumes that Confluence would never send an invalid macro ID...
      if (meBody) {
        // process and respond immediately for POST requests, since we have the body and should be able to assume
        // the macro ID is valid
        return processBody(meBody).then(handleContent).catch(macroFail(res, meName));
      } else {
        // renderingMethod:GET requests still need to go fetch the macro content
        getMacroBody(req.context, mePageId, mePageVersion, meContentId).then(function (result) {
          debug('request body:\n%s', result.body.body);
          return processBody(result.body.body);
        }).then(handleContent).catch(macroFail(res, meName));
      }
    }
  }

  function getMacroBody(context, page, version, id) {
    var uri = '/content/' + page + '/history/' + version + '/macro/id/' + id;
    debug('[MACRO] uri:', uri);
    return rest(context.http, {
      uri: uri,
      context: context,
      actAsUser: true
    });
  }

};
