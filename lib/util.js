const debuglog = require('util').debuglog('macro-requests');

const debug = exports.debug = function (...args) {
  debuglog(...args);
};

exports.etag = function (mePageId, mePageVersion, meContentId) {
  return new Buffer(hashCode(Array.from(arguments).join('|')).toString()).toString('hex');
};

function hashCode(s) {
  var hash = 0, i, chr;
  if (!s) return hash;
  for (i = 0; i < s.length; i++) {
    chr = s.charCodeAt(i);
    hash = ((hash << 5) - hash) + chr;
    hash |= 0; // convert to 32bit integer
  }
  return hash;
};

// this method fixes some seemingly jacked up or missing stuff with AC or in ACE's request middleware
exports.loadClientInfo = function (addon) {
  var authentication = require('atlassian-connect-express/lib/middleware/authentication');
  return function (req, res, next) {
    var clientKey = req.context.clientKey;
    if (!clientKey) {
      clientKey = authentication.extractUnverifiedClientKeyFromRequest(req, addon);
      req.context.clientKey = clientKey;
    }
    if (!clientKey) {
      next(new Error('No clientKey found in request'));
    }
    addon.settings.get('clientInfo', clientKey).then(function (clientInfo) {
      if (!clientInfo) {
        return next(new Error('No clientInfo found for clientKey: ' + clientKey));
      }
      req.context.clientInfo = clientInfo;
      next();
    }).catch(next);
  };
};

const macroError = exports.macroError = function (msg) {
  return '<p><strong>ERROR:</strong> ' + msg + '</p>';
};

exports.macroFail = function (res, meName) {
  return function (err) {
    debug('[MACRO] error:', meName, err.code, err.body);
    if (err.resmsg) {
      if (/^No macro found on content id/.test(err.resmsg)) {
        res.send(macroError(`Multiexcerpt "${meName}" not found.`));
      } else {
        res.send(macroError(err.resmsg));
      }
    } else if (err.code === 403) {
      res.send(macroError('Insufficient permissions.'));
    } else if (err.code === 404) {
      res.send(macroError('Not found.'));
    } else {
      var msg = err.stack || err;
      console.error(msg);
      res.send(500, msg);
    }
  };
};
