var NodeCache = require('node-cache');
var auth = require('./auth');

var actAsUserTokens = new NodeCache({stdTTL: auth.TTL});

module.exports = function (http, options) {
  return new Promise(function (resolve, reject) {
    var actAsUser = options.actAsUser;
    var promise = Promise.resolve();
    if (actAsUser) {
      var context = options.context;
      if (context.clientInfo.oauthClientId) {
        promise = promise.then(function () {
          return auth.getToken(actAsUserTokens.get(userAccountId(context)), {
            userAccountId: context.userAccountId,
            oauthClientId: context.clientInfo.oauthClientId,
            instanceBaseUrl: context.clientInfo.baseUrl,
            secret: context.clientInfo.sharedSecret
          });
        }).then(function (token) {
          actAsUserTokens.set(userAccountId(context), token);
          options.headers = options.headers || {};
          options.headers['Authorization'] = 'Bearer ' + token.value;
        });
      } else {
        reject(new Error('Cannot act-as-user without oauthClientId -- this add-on may require reinstallation'));
      }
    }
    promise.then(function () {
      return makeRequest(http, options);
    }).then(resolve, reject);
  });
};

function makeRequest(http, options) {
  return new Promise(function (resolve, reject) {
    options.uri = '/rest/api' + options.uri;
    options.json = true;
    var method = (options.method && options.method.toLowerCase()) || 'get';
    http[method](options, function (err, res, body) {
      if (err) {
        return reject(err);
      }
      if (res.statusCode >= 400) {
        var msg;
        if (typeof res.body === 'object') {
          msg = res.body.message ? res.body.message : JSON.stringify(body, null, 2);
        } else {
          msg = res.body;
        }
        err = new Error('Unexpected response code: ' + res.statusCode + (msg ? '\n' + msg : ''));
        err.response = res;
        err.code = res.statusCode;
        err.body = body;
        err.resmsg = msg;
        return reject(err);
      }
      resolve({
        response: res,
        body: body
      });
    });
  });
}

function userAccountId(context) {
  return context.userAccountId + '@' + context.clientInfo.baseUrl;
}
