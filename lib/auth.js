/**
 * Note: This auth code was written when Artemis still deployed all of its add-ons on ACEv1,
 * and we are aware that ACE3 provides it for us, we just haven't completely migrated.
 */

var request = require('request');
var jwt = require('atlassian-jwt');

var AUTHORIZATION_SERVER_URL = "https://auth.atlassian.io";
var EXPIRY_SECONDS = 60;
var REFRESH_THRESHOLD = 10;
var GRANT_TYPE = "urn:ietf:params:oauth:grant-type:jwt-bearer";
var SCOPES = "READ ACT_AS_USER"; // case-sensitive space-delimited as per the specification

function getNewToken(opts) {
    var now = Math.floor(Date.now() / 1000);
    var exp = now + EXPIRY_SECONDS;

    var jwtClaims = {
        iss: "urn:atlassian:connect:clientid:" + opts.oauthClientId,
        sub: "urn:atlassian:connect:useraccountid:" + opts.userAccountId,
        tnt: opts.instanceBaseUrl.replace(/\/$/, ''),
        aud: AUTHORIZATION_SERVER_URL,
        iat: now,
        exp: exp
    };

    var parameters = {
        grant_type: GRANT_TYPE,
        assertion: jwt.encode(jwtClaims, opts.secret),
        scope: SCOPES
    };

    return new Promise(function (resolve, reject) {
        request.post({
            url: AUTHORIZATION_SERVER_URL + '/oauth2/token',
            form: parameters,
            json: true,
            headers: {
                "Accept": "application/json"
            }
        }, function(err, res, body) {
            var statusCode = res.statusCode;
            if (err || statusCode < 200 || statusCode >= 300) {
                var msg = body ? JSON.stringify(body) : 'Failed to get OAuth access token';
                var error = err || new Error('[' + statusCode + '] ' + msg);
                reject(error);
            } else {
                resolve({
                    value: body.access_token,
                    type: body.token_type,
                    expiry: exp
                });
            }
        });
    });
}

function getValidToken(token, opts) {
    var now = Math.floor(Date.now() / 1000);
    if (!token || token.expiry - now <= REFRESH_THRESHOLD) {
        return getNewToken(opts);
    }
    return Promise.resolve(token);
}

exports.getToken = getValidToken;

exports.TTL = EXPIRY_SECONDS - REFRESH_THRESHOLD;
